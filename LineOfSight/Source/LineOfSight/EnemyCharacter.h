// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "EnemyCharacter.generated.h"

UCLASS()
class LINEOFSIGHT_API AEnemyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector lastSeenPlayerPos;
	FVector lastHeardPlayerPos;
	FVector lastSeenBodyPos;

	bool gunShotFired;

	float sightRadiusSqrd;
	float hearingRadiusSqrd;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// booleans to check for
	bool CheckSeePlayer(); // returns true if can see player
	bool CheckForGunshot(); // returns true if an hear gunshot
	bool CheckForBody(); // returns true if can see a dead body
	void Aim();
	void Shoot();
	void Turn(float degree);

	void Die();

	// location points to move to
	FVector GetLastSeenPlayerPos() { return lastSeenPlayerPos; }
	FVector GetLastHeardPlayerPos(){ return lastHeardPlayerPos; }
	FVector GetLastSeenBodyPos() { return lastSeenBodyPos; }

	UPROPERTY(EditDefaultsOnly, Category = ClassVariables)
	float sightRadius = 10000.0f;
	UPROPERTY(EditDefaultsOnly, Category = ClassVariables)
	float hearingRadius = 15000.0f;
	UPROPERTY(EditDefaultsOnly, Category = ClassVariables)
	float fieldOfView = 180.0f;
	UPROPERTY(EditDefaultsOnly, Category = ClassVariables)
	float turnRate = 180.0f;

	UPROPERTY(EditDefaultsOnly, Category = Animations)
	UAnimMontage* deathAnim;


	UPROPERTY(EditDefaultsOnly, Category = ClassVariables)
	TSubclassOf<AActor> ammoClass;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sound)
	class USoundBase* FireSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sound)
	class USoundBase* HuhSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Sound)
	class USoundBase* AlertSound;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	class UStaticMeshComponent* excl;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	class UStaticMeshComponent* ques;
	
	// set up for checking player gun shot
	void SetCheckGunshotToTrue() { gunShotFired = true; }
	
	void ShowQues();
	void HideQues();
	void ShowExcl();
	void HideExcl();

	void playHuhSound();
	void PlayAlertSound();

	UPROPERTY(EditAnywhere, Category = ClassVariables) bool patrolling;
	UPROPERTY(EditAnywhere, Category = ClassVariables) bool lockedPosition;
	FVector currPatrolEndpoint;
	UPROPERTY(EditAnywhere, Category = ClassVariables) AActor *patrolEndpoint1;
	UPROPERTY(EditAnywhere, Category = ClassVariables) AActor *patrolEndpoint2;

private:
	bool VisionHelper(FVector otherPos, FVector forwardVec);
	bool TracePlayer(bool vision, FVector forward);
	bool TraceEnemy(FVector forward);
	APawn* player;
	void DeactivateMesh();
	void RemoveBody();



};
