// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "LineOfSight.h"
#include "LineOfSightGameMode.h"
#include "LineOfSightHUD.h"
#include "LineOfSightCharacter.h"
#include "Engine.h"

ALineOfSightGameMode::ALineOfSightGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ALineOfSightHUD::StaticClass();

	PrimaryActorTick.bCanEverTick = true;
}

void ALineOfSightGameMode::BeginPlay() {
	for (TActorIterator<AEnemyCharacter> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		AEnemyCharacter* enemy = *ActorItr;
		aliveEnemies.insert(enemy);
	}
}

//Call this when an enemy is killed
void ALineOfSightGameMode::EnemyKilled(AEnemyCharacter* enemy) {
	aliveEnemies.erase(aliveEnemies.find(enemy));
	deadEnemies.insert(enemy);
}

//Call this when an enemy body despawns after a certain amount of time after death
void ALineOfSightGameMode::EnemyBodyDespawned(AEnemyCharacter* enemy) {
	deadEnemies.erase(deadEnemies.find(enemy));
}

void ALineOfSightGameMode::RestartLevel() {
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

void ALineOfSightGameMode::AllEnemiesCheckGunshot() {
	for (std::unordered_set<AEnemyCharacter* >::iterator it = aliveEnemies.begin(); it != aliveEnemies.end(); ++it) {
		AEnemyCharacter* enemy = *it;
		if (enemy) {
			enemy->SetCheckGunshotToTrue();
		}
	}
}

void ALineOfSightGameMode::KillPlayer() {

	playerIsDead = true;

	ACharacter* player = UGameplayStatics::GetPlayerCharacter(this, 0);
	player->GetMesh()->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);
	// enemies can no longer see player
	if (GEngine) {
		APlayerCameraManager* camera = GEngine->GetFirstLocalPlayerController(GetWorld())->PlayerCameraManager;
		camera->StartCameraFade(1.0f, 1.0f, 3.0f, FLinearColor::Black);
	}
	FTimerHandle RestartTimer;
	float timer = 2.0f;
	GetWorldTimerManager().SetTimer(RestartTimer, this, &ALineOfSightGameMode::RestartLevel,
		timer, false);

	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("player is (theoretically) killed"));

	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
	if (PC)
	{
		PC->SetCinematicMode(true, true, true);
	}
}

void ALineOfSightGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//print Enemies in Level
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, FString::FromInt(aliveEnemies.size()));

	if (aliveEnemies.size() <= 0)
	{
		allEnemiesDead = true;
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, TEXT("All Enemies Have Been Killed!"));
	}
}

int ALineOfSightGameMode::ReturnEnemyCount()
{
	return aliveEnemies.size();
}

bool ALineOfSightGameMode::ReturnPlayerIsDead() { return playerIsDead; }

bool ALineOfSightGameMode::ReturnAllEnemiesKilled() { return allEnemiesDead; }