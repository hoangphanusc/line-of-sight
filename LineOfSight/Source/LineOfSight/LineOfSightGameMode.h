// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "EnemyCharacter.h"
#include "EngineUtils.h"
#include <unordered_set>
#include "GameFramework/GameModeBase.h"
#include "LineOfSightGameMode.generated.h"

UCLASS(minimalapi)
class ALineOfSightGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALineOfSightGameMode();
	virtual void BeginPlay() override;
	void EnemyKilled(AEnemyCharacter* enemy);
	void EnemyBodyDespawned(AEnemyCharacter* enemy);
	std::unordered_set<AEnemyCharacter*>& getAliveEnemies() { return aliveEnemies; }
	std::unordered_set<AEnemyCharacter*>& getDeadEnemies() { return deadEnemies; }

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	int ReturnEnemyCount(); //Enemies Left in Level

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	bool ReturnPlayerIsDead();

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	bool ReturnAllEnemiesKilled();

	void RestartLevel();
	void KillPlayer();
	void AllEnemiesCheckGunshot();
	virtual void Tick(float DeltaTime) override;

private:
	std::unordered_set<AEnemyCharacter*> aliveEnemies;
	std::unordered_set<AEnemyCharacter*> deadEnemies;

	bool playerIsDead = false;
	bool allEnemiesDead = false;
};



