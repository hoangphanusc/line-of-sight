// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <string>
#include "AIController.h"
#include "EnemyCharacter.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class LINEOFSIGHT_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	void BeginPlay() override;
	void Tick(float deltaTime) override;
	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;
	void SetToDeadState();
	int32 GetState();
	UFUNCTION(BlueprintCallable, Category = "Jason")
		FString GetUState();
private:
	AEnemyCharacter* pawn;
	APawn* player;

	//-1 is right, 1 is left;
	int turnDir = 0;
	FTimerHandle LookAroundTimer;
	FTimerHandle AimTimer;
	FTimerHandle StartTurningTimer;
	FTimerHandle StopTurningTimer;

	//UPROPERTY(EditDefaultsOnly, Category = ClassVariables) float shootDelay;
	float shootDelay = 0.5f;

	bool aiming = false;

	void StopLookAround();
	void StartTurningRight();
	void StartTurningLeft();
	void StopTurning();

	enum State {
		PATROL, IDLE, DETECT, LOOKAROUND, AIM, DEAD
	};
	State currentState;
};
