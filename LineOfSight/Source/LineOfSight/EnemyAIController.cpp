// Fill out your copyright notice in the Description page of Project Settings.

#include "LineOfSight.h"
#include "Engine.h"
#include "EnemyAIController.h"

/* Stencil index mapping to PP_OutlineColored */
#define STENCIL_IDLE_OUTLINE 252;
#define STENCIL_ALERT_OUTLINE 253;
#define STENCIL_HOSTILE_OUTLINE 254;
#define STENCIL_DEAD_OUTLINE 255;

void AEnemyAIController::BeginPlay() {
	Super::BeginPlay();

	player = UGameplayStatics::GetPlayerPawn(this, 0);
	pawn = (AEnemyCharacter*)GetPawn();
	pawn->GetMesh()->SetRenderCustomDepth(true);

	//Super::BeginPlay();
	//Init state as patrol or idle, depending on the uproject variable set in blueprint
	if (pawn->patrolling) {
		currentState = PATROL;
		if (pawn->patrolEndpoint1 != nullptr && pawn->patrolEndpoint2 != nullptr) {
			pawn->currPatrolEndpoint = pawn->patrolEndpoint1->GetActorLocation();
		}
		else {
			pawn->patrolling = false;
			currentState = IDLE;
		}
	}
	else {
		currentState = IDLE;
	}
	
	aiming = false;
}


void AEnemyAIController::Tick(float deltaTime) {
	Super::Tick(deltaTime);
	if (GetPawn() == nullptr) {
		GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("pawn is null"));
		return;
	}
	
	//if (GEngine && currentState == AIM) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("Aiming"));
	//else if (GEngine && currentState == IDLE) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("Idling"));
	//else if (GEngine && currentState == PATROL) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("Patroling"));
	//else if (GEngine && currentState == DETECT) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("Detecting"));
	//else if (GEngine && currentState == LOOKAROUND) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("Looking around"));
	//else if (GEngine && currentState == DEAD) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("DEAD"));
	//else if (GEngine) GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Yellow, TEXT("Unrecognized State"));
	
	// Assign different action depending on the enemy's current state
	switch (currentState) {
	case PATROL:
		//GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, TEXT("PATROLLING"));
		//Set xray color
		pawn->GetMesh()->SetCustomDepthStencilValue((int32)252);

		//Check if enemy sees player
		if (pawn->CheckSeePlayer()) {
			currentState = AIM;
		}
		//Check if enemy heard a gunshot
		else if (pawn->CheckForGunshot()) {
			MoveToLocation(pawn->GetLastHeardPlayerPos());
			currentState = DETECT;
			pawn->playHuhSound();
			pawn->ShowQues();
		}
		//Check if enemy saw a body
		else if (pawn->CheckForBody()) {
			MoveToLocation(pawn->GetLastSeenBodyPos());
			currentState = DETECT;
			pawn->playHuhSound();
			pawn->ShowQues();
		}
		//Else move to the assigned patrol endpoint
		else MoveToLocation(pawn->currPatrolEndpoint);
		break;

	case IDLE:
		//GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, TEXT("IDLING"));
		//Set xray color
		pawn->GetMesh()->SetCustomDepthStencilValue((int32)252);

		//If enemy is supposed to be patrolling but is not (cause a move just completed, etc.), set to patrol
		if (pawn->patrolling) {
			currentState = PATROL;
		}

		//Check if the enemy can respond to suspicious activities (if the position is not locked)
		if (!pawn->lockedPosition) {
			//Check if enemy heard a gunshot
			if (pawn->CheckForGunshot()) {
				MoveToLocation(pawn->GetLastHeardPlayerPos());
				currentState = DETECT;
				pawn->playHuhSound();
				pawn->ShowQues();
			}
			//Check if enemy saw a body
			else if (pawn->CheckForBody()) {
				MoveToLocation(pawn->GetLastSeenBodyPos());
				currentState = DETECT;
				pawn->playHuhSound();
				pawn->ShowQues();
			}
		}

		//Check if enemy sees player
		if (pawn->CheckSeePlayer()) {
			currentState = AIM;
			pawn->ShowExcl();
		}

		break;

	case DETECT:
		//GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, TEXT("DETECTING"));
		
		//Set xray color
		pawn->GetMesh()->SetCustomDepthStencilValue((int32)253);
		
		//Check if enemy sees player
		if (pawn->CheckSeePlayer()) {
			currentState = AIM;
			pawn->HideQues();
			pawn->ShowExcl();
			pawn->PlayAlertSound();
		}
		//Check if enemy heard a gunshot
		else if (pawn->CheckForGunshot()){
			MoveToLocation(pawn->GetLastHeardPlayerPos());
		}
		//Check if enemy saw a body
		else if (pawn->CheckForBody()) {
			MoveToLocation(pawn->GetLastSeenBodyPos());
		}
		break;

	case LOOKAROUND:
		//GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, TEXT("LOOKING"));

		//Set xray color
		pawn->GetMesh()->SetCustomDepthStencilValue((int32)253);

		if (!StartTurningTimer.IsValid()) {
			GetWorldTimerManager().SetTimer(StartTurningTimer, this, &AEnemyAIController::StartTurningRight, 0.5f, false);
		}
		pawn->Turn(turnDir * pawn->turnRate * deltaTime);


		//If there's no timer (enemy just started looking around), set a timer for the enemy to stop looking.
		if (!LookAroundTimer.IsValid()) {
			GetWorldTimerManager().SetTimer(LookAroundTimer, this, &AEnemyAIController::StopLookAround, 4.0f, false);
		}

		//If at any point, the enemy sees the player, clear the lookaround timer & start aiming
		if (pawn->CheckSeePlayer()) {
			GetWorldTimerManager().ClearTimer(LookAroundTimer);
			GetWorldTimerManager().ClearTimer(StartTurningTimer);
			GetWorldTimerManager().ClearTimer(StopTurningTimer);
			currentState = AIM;
			pawn->ShowExcl();
			pawn->HideQues();
			pawn->PlayAlertSound();
		}
		break;

	case AIM:
		//Set xray color
		pawn->GetMesh()->SetCustomDepthStencilValue((int32)254);

		//Stop movement from chasing/patrolling
		StopMovement();		

		//Tell the enemy to aim (rotates them to the player)
		if (!pawn->lockedPosition) {
			pawn->Aim();
		}

		//If the enemy just entered aim state, set a timer to shoot
		if (!aiming) {
			aiming = true;
			GetWorldTimerManager().SetTimer(AimTimer, pawn, &AEnemyCharacter::Shoot, shootDelay, false);
		}

		//If the enemy lost sight of the player, clear the shoot timer, set state to detect
		// and move to the player's last seen position.
		if (!pawn->CheckSeePlayer()) {
			GetWorldTimerManager().ClearTimer(AimTimer);
			if (!pawn->lockedPosition) {
				MoveToLocation(pawn->GetLastSeenPlayerPos(), 10.0f);
				currentState = DETECT;
				pawn->ShowQues();
				pawn->HideExcl();
			}
			else {
				currentState = IDLE;
				pawn->HideExcl();
			}
			
			aiming = false;
		}
		break;

	case DEAD:
		//GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Red, TEXT("DEAD"));
		//Set xray color
		pawn->GetMesh()->SetCustomDepthStencilValue((int32)255);
		pawn->HideQues();
		pawn->HideExcl();
		break;

	}
}

void AEnemyAIController::StartTurningRight() {
	turnDir = -1.0f;
	GetWorldTimerManager().SetTimer(StopTurningTimer, this, &AEnemyAIController::StopTurning, 0.5f, false);
	GetWorldTimerManager().SetTimer(StartTurningTimer, this, &AEnemyAIController::StartTurningLeft, 1.0f, false);
}

void AEnemyAIController::StopTurning() {
	turnDir = 0;
}

void AEnemyAIController::StartTurningLeft() {
	turnDir = 1.0f;
	GetWorldTimerManager().SetTimer(StopTurningTimer, this, &AEnemyAIController::StopTurning, 1.0f, false);
}

void AEnemyAIController::StopLookAround() {
	if (!pawn->patrolling) {
		currentState = IDLE;
	}
	else {
		currentState = PATROL;
	}

	GetWorldTimerManager().ClearTimer(LookAroundTimer);
	pawn->HideQues();
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) {
	if (Result.IsSuccess()) {
		if (currentState == DETECT) {
			currentState = LOOKAROUND;
		}
		else if (currentState == PATROL) {
			if (pawn->currPatrolEndpoint == pawn->patrolEndpoint1->GetActorLocation()) {
				pawn->currPatrolEndpoint = pawn->patrolEndpoint2->GetActorLocation();
			}
			else {
				pawn->currPatrolEndpoint = pawn->patrolEndpoint1->GetActorLocation();
			}
			MoveToLocation(pawn->currPatrolEndpoint);
		}
	}
}

void AEnemyAIController::SetToDeadState() {
	StopMovement();
	GetWorldTimerManager().ClearTimer(LookAroundTimer);
	GetWorldTimerManager().ClearTimer(AimTimer);
	currentState = DEAD;
}

int32 AEnemyAIController::GetState() {
	switch (currentState) {
	case PATROL:
		return 252;
	case IDLE:
		return 252;
	case DETECT:
		return 253;
	case LOOKAROUND:
		return 253;
	case AIM:
		return 254;
	case DEAD:
		return 255;
	default:
		return 255;
	}
}

FString AEnemyAIController::GetUState() {
	std::string patrol = "Patrol", idle = "Idle", detect = "Detect";
	std::string lookaround = "Look Around", aim = "Aim", dead = "Dead", default = "";
	FString patrolF(patrol.c_str()), idleF(idle.c_str()), detectF(detect.c_str());
	FString lookaroundF(lookaround.c_str()), aimF(aim.c_str()), deadF(dead.c_str()), defaultF(default.c_str());
	switch (currentState) {
	case PATROL:
		return patrolF;
	case IDLE:
		return idleF;
	case DETECT:
		return detectF;
	case LOOKAROUND:
		return lookaroundF;
	case AIM:
		return aimF;
	case DEAD:
		return deadF;
	default:
		return defaultF;
	}
}