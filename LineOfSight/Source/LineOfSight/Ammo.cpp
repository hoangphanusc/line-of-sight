// Fill out your copyright notice in the Description page of Project Settings.

#include "LineOfSight.h"
#include "Ammo.h"
#include "LineOfSightCharacter.h"
#include "Engine.h"

// Sets default values
AAmmo::AAmmo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	collider = CreateDefaultSubobject<USphereComponent>(TEXT("AmmoCollider"));
	collider->SetSphereRadius(200.0f);
	collider->SetHiddenInGame(false);
	collider->SetCollisionResponseToChannel(ECC_Visibility, ECR_Ignore);
	//collider->bGenerateOverlapEvents = true;
	RootComponent = collider;

	
}

// Called when the game starts or when spawned
void AAmmo::BeginPlay()
{
	Super::BeginPlay();
	collider->OnComponentBeginOverlap.AddDynamic(this, &AAmmo::OnOverlapBegin);
}

// Called every frame
void AAmmo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void AAmmo::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	ALineOfSightCharacter* player = Cast<ALineOfSightCharacter>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComp != nullptr))
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("EnteredAmmo"));

		if (OtherActor == player)
		{
			player->CollectAmmo();
			if (CollectSound != NULL)
			{
				UGameplayStatics::PlaySoundAtLocation(this, CollectSound, GetActorLocation());
			}
			Destroy();
		}
	}
	
}

