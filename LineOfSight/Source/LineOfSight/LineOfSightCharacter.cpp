// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "LineOfSight.h"
#include "LineOfSightCharacter.h"
#include "LineOfSightProjectile.h"
#include "LineOfSightGameMode.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"
#include "Engine.h"
#include "String.h"
#include "EnemyCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// ALineOfSightCharacter

ALineOfSightCharacter::ALineOfSightCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void ALineOfSightCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	Ammo = 2; //set initial ammo count to 2 
}

//////////////////////////////////////////////////////////////////////////
// Input

void ALineOfSightCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);


	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ALineOfSightCharacter::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ALineOfSightCharacter::OnFire);
	}

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ALineOfSightCharacter::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &ALineOfSightCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ALineOfSightCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ALineOfSightCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ALineOfSightCharacter::LookUpAtRate);
}

void ALineOfSightCharacter::OnFire()
{

	// try and fire a projectile
	if (ProjectileClass != NULL && Ammo > 0)
	{
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(Ammo));

		static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
		static FName MuzzleSocket = FName(TEXT("Muzzle"));

		//CODE THAT USES CAMERA FWD VECTOR...NEEDS to be adjusted
		/*FVector Location, Direction;
		Cast<APlayerController>(GetController())->DeprojectMousePositionToWorld(Location, Direction);*/
		//DrawDebugLine(GetWorld(), Location, Location + (Direction * 100.0f), FColor::Blue, true);

		//Get Cam Position
		//FVector camPos = FirstPersonCameraComponent->GetComponentLocation();

		//Camera Fwd
		FVector fwdVec = FirstPersonCameraComponent->GetForwardVector();


		// Start from the muzzle's position
		FVector startPos = FP_Gun->GetSocketLocation(MuzzleSocket);

		// Calculate end position
		FVector EndPos = startPos + (fwdVec * WeaponRange);

		FHitResult* HitResult = new FHitResult();
		FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
		if (GetWorld()->LineTraceSingleByChannel(*HitResult, startPos, EndPos,
			ECollisionChannel::ECC_Pawn, TraceParams))
		{
			DrawDebugLine(GetWorld(), startPos, EndPos, FColor(255, 0, 0), true);

			//spawn particle effect at destination
			UGameplayStatics::SpawnEmitterAtLocation(this, particleSys, HitResult->ImpactPoint);
			AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(HitResult->GetActor());
			if (Enemy)
			{
				if (GEngine)
				{
					//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hit"));
				}
				Enemy->Die();
			}
			
		}
		
		//CODE THAT USES PLAYER FWD VECTOR
		//FVector startTrace = FirstPersonCameraComponent->GetComponentLocation();
		//FVector fwdVec = FirstPersonCameraComponent->GetForwardVector();
		//FVector Forward = GetActorForwardVector();
		// Start from the muzzle's position
		//FVector startPos = FP_Gun->GetSocketLocation(MuzzleSocket);
		// Calculate end position
		//FVector EndPos = startPos + (Forward * WeaponRange);

		// Perform trace to retrieve hit info
		//FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
		//TraceParams.bTraceAsyncScene = true;
		//TraceParams.bReturnPhysicalMaterial = true;
		//// This fires the ray and checks against all objects w/ collision
		//FHitResult Hit(ForceInit);
		//GetWorld()->LineTraceSingleByObjectType(Hit, startPos, EndPos,
		//	FCollisionObjectQueryParams::AllObjects, TraceParams);
		// Did this hit anything?
		//GetWorld()->DebugDrawTraceTag = WeaponFireTag;
		//if (Hit.bBlockingHit)
		//{
		//	UGameplayStatics::SpawnEmitterAtLocation(this, particleSys, Hit.ImpactPoint);
		//	AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(Hit.GetActor());
		//	if (Enemy)
		//	{
		//		if (GEngine)
		//		{
		//			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Hit"));
		//		}
		//		Enemy->Die();
		//	}
		//}

		// set all enemies to attempt gunshot detection
		ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
		if (gameMode) gameMode->AllEnemiesCheckGunshot();

	}

	// try and play the sound if specified
	if (FireSound != NULL && Ammo > 0)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
	else if (ReloadSound != NULL && Ammo <= 0)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ReloadSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL && Ammo > 0)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}

	Ammo -= 1; //subtract bullet

	if (Ammo < 0)
	{
		Ammo = 0;
	}

}

void ALineOfSightCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
	ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
	if (gameMode) gameMode->KillPlayer();
}

void ALineOfSightCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void ALineOfSightCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void ALineOfSightCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void ALineOfSightCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void ALineOfSightCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void ALineOfSightCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ALineOfSightCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool ALineOfSightCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ALineOfSightCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &ALineOfSightCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &ALineOfSightCharacter::TouchUpdate);
	}
	return bResult;
}

void ALineOfSightCharacter::KillEnemy(class AEnemyCharacter* hitTarget)
{
	hitTarget->Destroy();
}

void ALineOfSightCharacter::CollectAmmo()
{
	Ammo += 1;
}

void ALineOfSightCharacter::FellOutOfWorld(const class UDamageType& dmgType) {
	ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
	if (gameMode) gameMode->KillPlayer();
}