// Fill out your copyright notice in the Description page of Project Settings.

#include "LineOfSight.h"
#include "EnemyAIController.h"
#include "LineOfSightCharacter.h"
#include "LineOfSightGameMode.h"
#include "Engine.h"
#include "EnemyCharacter.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AIControllerClass = AEnemyAIController::StaticClass();
	excl = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EXCL"));
	excl->SetupAttachment(RootComponent);
	excl->SetRenderCustomDepth(true);
	excl->SetCustomDepthStencilValue((int32)254);
	ques = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("QUES"));
	ques->SetupAttachment(RootComponent);
	ques->SetRenderCustomDepth(true);
	ques->SetCustomDepthStencilValue((int32)253);
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	player = UGameplayStatics::GetPlayerPawn(this, 0);

	sightRadiusSqrd = FMath::Pow(sightRadius, 2.0f);
	hearingRadiusSqrd = FMath::Pow(hearingRadius, 2.0f);

	HideExcl();
	HideQues();

}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool AEnemyCharacter::VisionHelper(FVector otherPos, FVector forwardVec) {


	float distanceSqrd = FVector::DistSquared(otherPos, GetActorLocation());
	// player is too far to see
	if (distanceSqrd > sightRadiusSqrd)  return false;

	// use dot product to find if player is in field of view
	FVector actorForward = GetActorForwardVector();
	actorForward.Normalize();
	float dotProduct = FVector::DotProduct(forwardVec, actorForward);;
	float angle = FGenericPlatformMath::Acos(dotProduct);
	angle = FMath::RadiansToDegrees(angle);
	
	if (angle > fieldOfView) {
		return false;
	}
	return true;

}

bool AEnemyCharacter::CheckSeePlayer(){

	// find the distance between this and the player
	FVector playerPos;
	if (player) playerPos= player->GetActorLocation();
	FVector forwardVector = playerPos - GetActorLocation();// get forward vector and player position
	
	if (VisionHelper(playerPos, forwardVector)) {// check if it player is in fov
		if (TracePlayer(true, forwardVector)) {// check if player is behidn cover or not
			lastSeenPlayerPos = playerPos;		// if not, return true
			return true;
		}
	}
	return false;

}

bool AEnemyCharacter::CheckForGunshot(){
	if (gunShotFired) {
		gunShotFired = false;
		FVector playerPos;
		// get player location
		if (player) playerPos = player->GetActorLocation();
		float distanceSqrd = FVector::DistSquared(playerPos, GetActorLocation());
		if (distanceSqrd < hearingRadiusSqrd) {
			lastHeardPlayerPos = playerPos;
			return true;
		}
	}
	return false;
}

bool AEnemyCharacter::CheckForBody(){
	ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
	if (gameMode) {
	// go through list of bodies for each item
		for (AEnemyCharacter* body : gameMode->getDeadEnemies()) {
			// get their position, and the vector from this to them
			FVector bodyPos = body->GetActorLocation();
			FVector dirVector = bodyPos - GetActorLocation();
			// run vision helper
			if (VisionHelper(bodyPos, dirVector)) {
				if (TraceEnemy(dirVector)) { // if vision returns true, run trace enemy
					lastSeenBodyPos = bodyPos;
					// start timer to remove body
					float timer = 1.0f;
					FTimerHandle bodyRemoveTimer;

					GetWorldTimerManager().SetTimer(bodyRemoveTimer, body, &AEnemyCharacter::RemoveBody,
						timer, false);

					return true;
				}
			}
			// return true
		}

	}
	return false;
}

void AEnemyCharacter::Aim(){
	FVector storePos;
	if (player) storePos = player->GetActorLocation();
	storePos = storePos - GetActorLocation();
	storePos.Normalize();

	SetActorRotation(storePos.Rotation());

}

void AEnemyCharacter::Shoot() {
	ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
	if (gameMode) gameMode->KillPlayer();

	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}
}

void AEnemyCharacter::Turn(float degree) {
	FVector forward = GetActorForwardVector();
	forward = forward.RotateAngleAxis(degree, GetActorUpVector());
	FRotator rot = forward.Rotation();
	SetActorRotation(rot);
}

// see if you can hit the player
bool AEnemyCharacter::TracePlayer(bool vision, FVector forward) {

	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));
	// raycast vector setup
	FHitResult hitResult = FHitResult();
	FVector StartTrace = GetActorLocation();
	FVector EndTrace = forward*sightRadius + StartTrace;
	
	FCollisionQueryParams TraceParams = FCollisionQueryParams(WeaponFireTag);

	// set collision channel based on whether enemy is shooting player or just looking
	ECollisionChannel channel = ECC_EngineTraceChannel1;
	if (vision) channel = ECC_Visibility;

	// perform trace
	GetWorld()->LineTraceSingleByChannel(hitResult, StartTrace, EndTrace,
		channel, TraceParams);

	if (hitResult.bBlockingHit) {
		// check to see if the hit thing is a player
		ALineOfSightCharacter* player = Cast<ALineOfSightCharacter>(hitResult.GetActor());
		if (player)
		{
			return true;
		}
	}
	return false;
}

bool AEnemyCharacter::TraceEnemy(FVector forward) {

	// raycast vector setup
	FHitResult hitResult = FHitResult();
	FVector StartTrace = GetActorLocation();
	FVector EndTrace = forward*sightRadius + StartTrace;

	FCollisionQueryParams TraceParams = FCollisionQueryParams();
	
	// perform trace
	GetWorld()->LineTraceSingleByChannel(hitResult, StartTrace, EndTrace,
		ECC_Visibility, TraceParams);
	if (hitResult.bBlockingHit) {
		// check to see if the hit thing is an enemy
		AEnemyCharacter* enemy = Cast<AEnemyCharacter>(hitResult.GetActor());
		if (enemy)
		{
			return true;
		}
		else return false;
	}
	else return false;
}

void AEnemyCharacter::Die() {
	// spawn ammo at my location
	GetWorld()->SpawnActor<AActor>(ammoClass, GetActorLocation(), GetActorRotation());

	// set state to dead
	AEnemyAIController* ctrlr = (AEnemyAIController*)GetController();
	if (ctrlr) {
		ctrlr->SetToDeadState();
	}
	// disable trace channel for gunshots and enable for dead bodies
	GetMesh()->SetCollisionResponseToChannel(ECC_EngineTraceChannel1, ECR_Ignore); // weapons
	// disable collision for pawns
	GetCapsuleComponent()->SetCollisionProfileName(FName("Spectator"));

	// play death animation
	FTimerHandle DeathTimer;
	float timer = PlayAnimMontage(deathAnim) - 0.25f;
	GetWorldTimerManager().SetTimer(DeathTimer, this, &AEnemyCharacter::DeactivateMesh,
		timer, false);

	// add this to list of things that are dead
	ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
	if (gameMode) gameMode->EnemyKilled(this);
}

void AEnemyCharacter::DeactivateMesh() {
	GetMesh()->Deactivate();
	GetMesh()->SetCollisionProfileName(FName("Spectator"));
}

void AEnemyCharacter::RemoveBody() {
	ALineOfSightGameMode* gameMode = (ALineOfSightGameMode*)GetWorld()->GetAuthGameMode();
	if (gameMode) gameMode->EnemyBodyDespawned(this);
	Destroy();
}


void AEnemyCharacter::ShowQues(){
	if (ques != NULL) {
		ques->SetVisibility(true);
	}
}
void AEnemyCharacter::HideQues(){
	if (ques != NULL) {
		ques->SetVisibility(false);
	}
}
void AEnemyCharacter::ShowExcl(){
	if (excl != NULL) {
		excl->SetVisibility(true);
	}
}
void AEnemyCharacter::HideExcl(){
	if (excl != NULL) {
		excl->SetVisibility(false);
	}

}

void AEnemyCharacter::playHuhSound(){
	if (HuhSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, HuhSound, GetActorLocation());
	}
}
void AEnemyCharacter::PlayAlertSound(){
	if (AlertSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, AlertSound, GetActorLocation());
	}
}